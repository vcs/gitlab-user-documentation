# GitLab User Documentation

User facing documentation intended for internal user of the personnel at CERN.

This documentation is deployed under <https://gitlab.docs.cern.ch>

## Develop this documentation

To develop this documentation, we can:

```bash
docker run -it --rm -p 3000:3000 --entrypoint=/bin/bash -v "C:\path\to\gitlab-user-documentation\:/documentation" -w "/documentation" registry.cern.ch/docker.io/library/node:18
root@xxx/documentation: cd docs
root@xxx:/documentation/docs# yarn start -h 0.0.0.0

# If new version of Docusaurus, consider upgrading with (follow server instructions):
# ffi: https://docusaurus.io/docs/installation#updating-your-docusaurus-version
#
# yarn upgrade @docusaurus/core@latest @docusaurus/preset-classic@latest

# If necessary, consider delete node_modules and package-lock.json files, then npm install for a fresh install of dependencies
# ffi: https://docusaurus.io/docs/migration/v3#troubleshooting
```

## Upstream documentation

For further information about how this documentation has been built, refer to [https://docusaurus.io/docs](https://docusaurus.io/docs).
