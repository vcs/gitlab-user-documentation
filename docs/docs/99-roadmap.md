---
title: Roadmap
---

This roadmap outlines our current and upcoming activities for the current and next year, giving you a clear picture of the GitLab team's workload.

:::note

This roadmap is a flexible guide and times are rough estimates. GitLab's Team may need to adjust actions based on the Group/Department's evolving needs. We'll keep this roadmap updated to reflect any changes.

:::

The roadmap is grouped by colors, and each color represents the actions that will be taken per each of the key elements in the GitLab infrastructure:

- **Blue - GitLab Application**: work related to the GitLab Application, improvements and features.
- **Red - GitLab Runners**: work related to provision, improve, etc. the pool of GitLab Runners.
- **Purple - GitLab Releases**: estimated time where a new release of GitLab will be deployed at CERN.
- **Green - Actions Required (by users)**: it highlights times throughout the year when user actions will be needed to achieve a specific goal. Notifications will be sent.

To visit the Git Service Roadmap, navigate to:

- [https://cern.ch/git-service-roadmap](https://cern.ch/git-service-roadmap)

## Feedback

To send us suggestions or feedback, please go to the [Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)