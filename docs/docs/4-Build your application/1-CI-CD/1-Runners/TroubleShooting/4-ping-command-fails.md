# Ping command fails in kubernetes runners

When a user tries to execute the `ping` command from a kubernetes runner the following message is shown.

```bash
ping google.com
# bash: /usr/bin/ping: Operation not permitted
```

## When this happens?

This behavior has been noticed for jobs running on **Ubuntu** based images with version `20.04` and `22.04` (the ones tested. Notice that other minor versions may be potentially affected as well).

## Solution

The solution to this issue is to run the following command to set root as the owner of the executable command, even if root user is already the owner.

```bash
chown root:root /usr/bin/ping
```

Therefore if you need to add this workaround to your jobs, you can do as follows:

```yaml
job:
  script:
  - chown root:root /usr/bin/ping
  - echo "other commands start"

```bash
ping google.com
# PING google.com (216.58.215.238) 56(84) bytes of data.
# 64 bytes from zrh11s02-in-f14.1e100.net (216.58.215.238): icmp_seq=1 ttl=55 time=6.02 ms
# 64 bytes from zrh11s02-in-f14.1e100.net (216.58.215.238): icmp_seq=2 ttl=55 time=5.99 ms
# 64 bytes from zrh11s02-in-f14.1e100.net (216.58.215.238): icmp_seq=3 ttl=55 time=6.17 ms
# ^C
# --- google.com ping statistics ---
# 3 packets transmitted, 3 received, 0% packet loss, time 2003ms
# rtt min/avg/max/mdev = 5.994/6.061/6.173/0.079 ms

```
