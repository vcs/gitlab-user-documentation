# Troubleshooting

The following section will contain examples and solutions to known issues reported by users, and detected while running in the new runners based on Kubernetes.

Found a new one? Please submit a ticket with us [through the Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service) and tell us about it!
