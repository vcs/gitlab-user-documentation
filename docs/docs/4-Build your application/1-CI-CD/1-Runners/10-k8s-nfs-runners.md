---
title: Kubernetes+executor NFS Runners
---

A new fleet of runners has been deployed at CERN, centrally managed and using what is called the [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html), targeting NFS workloads on the Technical Network (TN)

NFS runners are specialized runners that provide CI/CD jobs with access to the TN-based NFS servers (cs-ccr-nfsop, cs-ccr-nfshome, etc...).

These runners are meant to be used for deploying to NFS. Not for jobs than can also be done on the GPN / default runners.

## Prerequisites

The TN runners have stricter access control than the other runners. Accounts must meet all prerequisites:

- A CERN primary account. No secondary or service accounts.
- Member of `acc-all` - default for ATS members, otherwise request to [acc-adm-support@cern.ch](mailto:acc-adm-support@cern.ch)
- Member of `nfs-runner-users` - request access at [https://groups-portal.web.cern.ch/group/nfs-runner-users/details](https://groups-portal.web.cern.ch/group/nfs-runner-users/details)by clicking the blue "Subscribe" button. Please provide a descriptive reason in the request.

## Usage

In order to start using the K8s NFS Runners, a user needs to add one the tags mentioned below to its `.gitlab-ci.yml` configuration file.

- `nfs`
- `k8s-nfs`

As an example, you can use:

```yaml
job:
  tags:
    - nfs # or k8s-nfs
  script:
    - echo "write something to NFS as your own user (whom executes the job)"
```

It is also to impersonate a service account, rather than writing to NFS as your own user. 
For this, the service accounts must be linked to an egroup in [the service-account-mapping repository](https://gitlab.cern.ch/acc-adm/devops/gitlab/tn-runners/service-account-mapping/-/blob/master/map.yml?ref_type=heads).
This can be requested by creating a MR (from a fork), or [creating an issue](https://gitlab.cern.ch/acc-adm/devops/gitlab/tn-runners/service-account-mapping/-/issues/new)

To impersonate a service account in the Job, you can set the `NFS_USER` variable:

```yaml
job:
  tags:
    - nfs # or k8s-nfs
  variables:
    NFS_USER: myserviceaccount
  script:
    - echo "write something to NFS as 'myserviceaccount'"
```

The group owner of the files will be set to the GID of the service account.

## General considerations for Kubernetes+executor NFS Runners

| Specs                | Value                                                                                                              |
|----------------------|--------------------------------------------------------------------------------------------------------------------|
| Default image        | AlmaLinux9                                                                                                         |
| CVMFS available      | ❌                                                                                                                  |
| EOS available        | ❌                                                                                                                  |
| NFS available        | ✅                                                                                                                  |
| Internet access      | ❌                                                                                                                  |
| GPN access           | ❌                                                                                                                  |
| TN access            | ❌                                                                                                                  |
| Privileged           | ❌                                                                                                                  |
| Runner configuration | [K8s NFS Runners](https://gitlab.cern.ch/vcs/gitlab-runners/-/blob/master/chart/environments/k8s-nfs-dev-v28.yaml) |

These K8s NFS Runners use the [`Kubernetes-executor`](https://docs.gitlab.com/runner/executors/kubernetes.html). This means that jobs executed by these runners will run on a dedicated Kubernetes cluster, aiming at improving scalability, resource optimization, isolation and portability.

The set of K8s NFS Runners will run as **`non-privileged`**, meaning that `docker` commands are unlikely to work.

Regarding the image, these runners use the **AlmaLinux9** image by default (concretely `gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9:latest`)
Users are allowed to replace the image at their best convenience, as it was the case nowadays by setting the [`image`](https://docs.gitlab.com/ee/ci/yaml/#image) keyword appropriately.

Please note that only registry-tn.cern.ch and public/internal projects on gitlab-registry.cern.ch are available in the TN.
To make a custom image available in the TN see: [https://acc-containers.docs.cern.ch/workflow/controls/](https://acc-containers.docs.cern.ch/workflow/controls/)

An example of using an image other than the default one provided is:

```yaml
job:
  image: registry-tn.cern.ch/acc/my-group/my-project/my-image:my-tag
```


## Fair use

:::info

Please, remember that these runners are **shared among many users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
