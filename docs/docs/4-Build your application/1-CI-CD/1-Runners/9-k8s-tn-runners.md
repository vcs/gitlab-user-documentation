---
title: Kubernetes+executor TN Runners
---

A new fleet of runners has been deployed at CERN, centrally managed and using what is called the [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html), targeting TN workloads.

TN runners are specialized runners that can execute CI/CD jobs on the Technical Network (TN).

These runners are meant to be used for testing or deploying against devices on the TN. Not for jobs than can also be done on the GPN, like compilation.

## Prerequisites

The TN runners have stricter access control than the other runners. Accounts must meet all prerequisites:

- A CERN primary account. No secondary or service accounts.
- Member of `acc-all` - default for ATS members, request to [acc-adm-support@cern.ch](mailto:acc-adm-support@cern.ch)
- Member of `tn-runner-users` - request access at [https://groups-portal.web.cern.ch/group/tn-runner-users/details](https://groups-portal.web.cern.ch/group/tn-runner-users/details) by clicking the blue "Subscribe" button. Please provide a descriptive reason in the request.

## Usage

In order to start using the K8s TN Runners, a user needs to add one the tags mentioned below to its `.gitlab-ci.yml` configuration file.

- `tn`
- `k8s-tn`

As an example, you will use:

```yaml
job:
  tags:
    - tn # or k8s-tn
  script:
    - echo "do something in the Technical Network..."
```

## General considerations for Kubernetes+executor TN Runners

| Specs                | Value                                                                                                            |
|----------------------|------------------------------------------------------------------------------------------------------------------|
| Default image        | AlmaLinux9                                                                                                       |
| CVMFS available      | ❌                                                                                                                |
| EOS available        | ❌                                                                                                                |
| NFS available        | ❌                                                                                                                |
| Internet access      | ❌                                                                                                                |
| GPN access           | ❌                                                                                                                |
| TN access            | ✅                                                                                                                |
| Privileged           | ❌                                                                                                                |
| Runner configuration | [K8s TN Runners](https://gitlab.cern.ch/vcs/gitlab-runners/-/blob/master/chart/environments/k8s-tn-dev-v28.yaml) |

These K8s TN Runners use the [`Kubernetes-executor`](https://docs.gitlab.com/runner/executors/kubernetes.html). This means that jobs executed by these runners will run on a dedicated Kubernetes cluster, aiming at improving scalability, resource optimization, isolation and portability.

Regarding the image, these runners use the **AlmaLinux9** image by default (concretely `gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9:latest`). Users are allowed to replace the image at their best convenience, as it was the case nowadays by setting the [`image`](https://docs.gitlab.com/ee/ci/yaml/#image) keyword appropriately.
Note that only registry-tn.cern.ch and public project on gitlab-registry.cern.ch are available in the TN.
To make a custom image available in the TN see: [https://acc-containers.docs.cern.ch/workflow/controls/](https://acc-containers.docs.cern.ch/workflow/controls/)

An example of using an image other than the default one provided is:

```yaml
job:
  image: registry-tn.cern.ch/acc/my-group/my-project/my-image:my-tag
```

The set of K8s TN Runners will run as **`non-privileged`**, meaning that `docker` commands are unlikely to work.

## Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
