---
title: Private GitLab Runners registration
---

Apart from the instance GitLab Runners provided by the GitLab team for everyone, users can have their own dedicated project or group GitLab runners.
As from GitLab version 17.0, the runner registration flow will change and it will require generating a runner token for each runner.
This is mainly done, among many other reasons, for security reasons, to not have a global registration token that in case of a leak it will need to be updated for all runners.

:::info

For more info check the documentation here: [https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#the-new-runner-registration-workflow](https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#the-new-runner-registration-workflow)

:::

### Register a project/group runner

1. For project runners, go to you project under **Settings** > **CI/CD** . Click on the **Runners** section to expand and finally click on **New project runner**. For Group Runners, go to your group's main menu and then **Build** > **Runners**.

    ![Create project runner location](/img/assets/runner-registration/create_project_runner_location.png)

2. Pick the **Operating System** of your runner machine and specify the **Tags**

    ![Create runner](/img/assets/runner-registration/create_runner.png)

3. Finally, retrieve the token or the full command from the last screen and run it in your machine.

    ![Retrieve runner token](/img/assets/runner-registration/retrieve_runner_token.png)

### Adapt your registration arguments when migrating from Registration Tokens

Either you are using a script, a helm chart or a docker installation for your runner you must update your configuration since certain fields are deprecated. Together with registration tokens the following field are being obsolete since they are being provided in UI registration flow we presented above.

- `--locked`
- `--access-level`
- `--run-untagged`
- `--maximum-timeout`
- `--paused`
- `--tag-list`
- `--maintenance-note`

:::info

If you amend to remove any of the fields above the following error message will be shown during the runner registration process.

  ```bash
  FATAL: Runner configuration other than name and executor configuration is reserved (specifically --locked, --acces s-level, --run-untagged, --maximum-timeout, --paused, --tag-list, and --maintenance-note) and cannot be specified when registering with a runner authentication token. This configuration is specified on the GitLab server. Please try again without specifying any of those arguments.
  ```

For more information have a look in the [gitlab deprecation changelog](https://docs.gitlab.com/ee/update/deprecations.html#registration-tokens-and-server-side-runner-arguments-in-gitlab-runner-register-command)
:::
