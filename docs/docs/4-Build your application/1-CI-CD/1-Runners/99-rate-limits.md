---
title: Rate Limits - GitLab Runners
---

Throttling allows GitLab Service Administrators to effectively manage the resources of our GitLab runners infrastructure. By setting throttling limits, we can control the maximum number of concurrent jobs or pipelines that can be executed at a given time. This ensures that our infrastructure is not overwhelmed by an excessive workload, resulting in better resource utilization and improved overall performance. When there are too many concurrent jobs running simultaneously, it can lead to resource bottlenecks, reduced performance, and potential failures, as it happened in the past.

In brief, setting rate limits or throttling the infrastructure can provide several benefits, such as efficient resource allocation, maintaining system performance and stability, preventing resource exhaustion, cost optimization by controlling resource usage and enhancing security and compliance measures.

Thus, GitLab Service at CERN have set a series of limits to avoid misuse and/or abuse of the Runners infrastructure, that will be published in these document for your information.

:::info

Note that the following limits are applicable regardless of shared/private runners. **All runners have these limits**.

:::

## Number of jobs in active pipelines

[Number of jobs in active pipelines](https://docs.gitlab.com/ee/administration/instance_limits.html#number-of-jobs-in-active-pipelines) limit is set to `200`.
