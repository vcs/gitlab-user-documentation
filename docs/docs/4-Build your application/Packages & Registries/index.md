---
title: Packages & Registries
---

## Overview

GitLab features and integrated [Docker image registry](https://github.com/docker/distribution). It allows users to keep their Docker images tied together with their source code and Dockerfile. Also automatic tools to build your images and upload them to the registry, using GitLab-CI are provided.

**Update March 2021**: Package registry is enabled globally in the infrastructure. Documentation about this feature can be found at [Gitlab Package Registry](https://docs.gitlab.com/ee/user/packages/).
