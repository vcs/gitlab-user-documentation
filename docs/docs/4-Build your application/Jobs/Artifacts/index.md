# Job Artifacts

Jobs can output an archive of files and directories. This output is known as a [**job artifact**](https://docs.gitlab.com/ee/ci/jobs/).

These job artifacts, by default and for historical reasons, were always kept for sucessful pipelines for the most recent commit on each ref. However, as of November 2024, and due to constrainst in the storage backend (see [OTG0152231](https://cern.service-now.com/service-portal?id=outage&n=OTG0152231) for reference), the Git Service, in collaboration with the Storage Service, have decided to no longer keep artifacts from most recent successful jobs.

:::info

Keeping the latest artifacts can use a large amount of storage space in projects with a lot of jobs or large artifacts. **Use them with reponsibility**.

:::

As such, **all projects** under GitLab at CERN will have the `Keep artifacts from most recent successful jobs` setting disabled. Job artifacts will then expire as per the default expiration time of the job artifacts set in the Admin area. The default artifacts expiration time for job artifacts is set to `30 days`.

After the `Keep artifacts from most recent successful jobs` setting has been disabled, all new artifacts expire according to the [expire_in](https://docs.gitlab.com/ee/ci/yaml/#artifactsexpire_in) configuration. Artifacts in old pipelines continue to be kept until a new pipeline runs for the same ref. Then the artifacts in the earlier pipeline for that ref are allowed to expire too.

## Maximum artifact size

The maximum artifact size in our GitLab installation is set globally to `2048 MB` (2GB). In the cases that this value needs to be increased per group/project-basis, consider [submitting a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) with us. Only GitLab administrators can increase this value (c.f. https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#maximum-artifacts-size).

:::warning

By default, job artifacts are transferred to all downstream stages, hence big artifacts will delay the startup of these downstream jobs as a consequence.

Bear this in mind when asking for increasing the maximum artifacts size of your project(s), and consider the infrastructure impact when requesting larger artifacts size.

:::

## Managing job artifacts

With the default values in place, users are encouraged to make a responsible usage of the artifacts, and only keep those job artifacts that are needed for the interest of the project and/or the Organization. As mentioned, and by default, these job artifacts will be kept `30 days` for successful pipelines.

For the list of actions that you can do regarding job artifacts, please refer to [Job artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html). You will find documentation about how to create job artifacts, prevent a job from fetching artifacts, etc., among others.

There are other operations that are interesting to know as per the default values set in the GitLab at CERN instance, described below.

### Preserve job artifacts beyond the default artifacts expiration date

In order to preserve job artifacts beyond the default artifacts expiration date (30 days), the use of `expire_in` becomes mandatory. The `expire_in` keyword determines for how long GitLab should keep the artifacts. For example:

```yaml
pdf:
  script: xelatex mycv.tex
  artifacts:
    paths:
      - mycv.pdf
    expire_in: 1 week
```

If `expire_in` is not defined, the default artifacts expiration value of `30 days` applies.

### Clean up old existing job artifacts

The change of the `Keep artifacts from most recent successful jobs` setting will apply to newly created job artifacts. However, and for years, this provoked an accumulation of job artifacts that will stay there forever if no action is taken.

As such, the GitLab Team has prepared some recipes for users in order to clean up old artifacts from their repositories. You can opt for performing these operations:

- Via the GitLab UI from the Artifacts page.
- Via a python script.

:::danger

These are **destructive actions** that cannot be reverted. Use with caution.

:::

#### Via the GitLab UI from the Artifacts page

Go to your project > (on the left) Build > Artifacts. [You can delete artifacts one by one or in bulk](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#bulk-delete-artifacts).

#### Via a python script

:::note

If your project has a several thousand job artifacts, kindly reach out to us for cleaning up the artifacts by [submitting a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service).

:::

To perform the clean up via a python script, below it is outlined the requirements and step needed.

:::warning

Make sure you use `--help` to understand its usage, and you first use `--dry-run` to simulate an execution.

:::

##### Requirements and pre-requisites

- [Python 3.8+](https://www.python.org/downloads/windows/)
- [python-gitlab](https://pypi.org/project/python-gitlab/): `pip install python-gitlab`
- [requests](https://pypi.org/project/requests/): `pip install requests`
- Docker (Optional, to execute the script in a container)
- A [Personal Access](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) / [Project access](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) / [Group access](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) tokens with scope `api, read_api`.
- The person that is going to execute the script, must have a **`Maintainer`** role for the target project.
- The Python script that will execute the clean up.
- The `PROJECT_ID` of the project to act upon. To pick the id up, go to your project > (on the right) Settings > General > pick the `Project ID` value.

##### Python script

The python script that will perform the clean up. Copy https://gitlab.cern.ch/vcs/gitlab-user-documentation/-/blob/master/utilities/cleanup/gitlab_cleanup_projects.py?ref_type=heads and paste it in your local environment (host machine or Docker).

##### Steps to setup the script from Host Machine

- Install the requirements.
- Create a Python file in your local machine and copy the contents of the Script.
- Run `python gitlab_cleanup_projects.py --help` for usage instructions.

##### Steps to Run the script from Docker Container

- Install Docker.
- Create a Python file in your local machine and copy the contents of the Script.
- Spin up a Docker Python container and run:

For Linux:

```bash
# create the file and paste the content of the python script mentioned above
# vim gitlab_cleanup_projects.py

docker run -it --rm -v $(pwd):/gitlab-script -w /gitlab-script python:3.10 sh -c "pip install python-gitlab requests && python *.py --help && bash"
```

For PowerShell:

```powershell
# create the file and paste the content of the python script mentioned above
# code gitlab_cleanup_projects.py

docker run -it --rm -v ${PWD}:/gitlab-script -w /gitlab-script python:3.10 sh -c "pip install python-gitlab requests && python *.py --help && bash"
```

##### Run the script

Below you will find steps to run the script regardless of the environment, as well as examples that may be useful for you.

Example: **Dry run to cleanup artifacts before 1st January 2020.**

Command:

```bash
export GITLAB_ACCESS_TOKEN="your-pat-project-group-token" # e.g. "gpat-123abc"
PROJECT_ID=123
DATE="2020-01-01"

python3 gitlab_cleanup_projects.py --project $PROJECT_ID --cleanup artifacts --date $DATE --dry-run

# Output/Log:
# Authenticated successfully with https://gitlabstg.cern.ch
# Running in Dry Run mode. No actual deletions will be performed.
# Fetching GitLab project(s)...
# Found 1 project(s) for cleanup. Starting cleanup operations...
# -- Scanning project susuara/playground-project-1 (ID: 910) (URL: https://gitlabstg.cern.ch/susuara/playground-project-1) for cleanup...
# No jobs with artifacts older than 2020-01-01 found in project susuara/playground-project-1
# Finished cleaning up GitLab project(s).
```

A dry run will show you what actions will be taken without performing the actions.

Example: **Cleanup artifacts before 3rd October 2024.**

Command:

```bash
export GITLAB_ACCESS_TOKEN="your-pat-project-group-token" # e.g. "gpat-123abc"
PROJECT_ID=910
DATE="2024-10-03"

python3 gitlab_cleanup_projects.py --project $PROJECT_ID --cleanup artifacts --date $DATE

# Output/Log:
# Authenticated successfully with https://gitlabstg.cern.ch
# Fetching GitLab project(s)...
# Found 1 project(s) for cleanup. Starting cleanup operations...
# -- Scanning project susuara/playground-project-1 (ID: 910) (URL: https://gitlabstg.cern.ch/susuara/playground-project-1) for cleanup...
# Job #31730 created on 2024-10-02 is older than 2024-10-03 and is marked for deletion. Job URL: https://gitlabstg.cern.ch/susuara/playground-project-1/-/jobs/31730
# Job #31728 created on 2024-09-27 is older than 2024-10-03 and is marked for deletion. Job URL: https://gitlabstg.cern.ch/susuara/playground-project-1/-/jobs/31728
# Deleting artifacts for 2 jobs in project susuara/playground-project-1...
# Deleted artifacts for Job #31730
# Deleted artifacts for Job #31728
# Finished cleaning up GitLab project(s).
```

Example: **Cleanup pipelines before 1st January 2023.**

Command:

```bash
export GITLAB_ACCESS_TOKEN="your-pat-project-group-token" # e.g. "gpat-123abc"
PROJECT_ID=910
DATE="2024-01-01"

python3 gitlab_cleanup_projects.py --project $PROJECT_ID --cleanup pipelines --date $DATE

# Output/Log:
# Authenticated successfully with https://gitlabstg.cern.ch
# Fetching GitLab project(s)...
# Found 1 project(s) for cleanup. Starting cleanup operations...
# -- Scanning project susuara/playground-project-1 (ID: 910) (URL: https://gitlabstg.cern.ch/susuara/playground-project-1) for cleanup...
# ...
# Checking pipeline #8337361 created on 2024-10-17 | Total no. of pipelines scanned: 3534
# Checking pipeline #8337372 created on 2024-10-17 | Total no. of pipelines scanned: 3535
# No pipelines older than 2023-01-01 found in project susuara/playground-project-1
# Finished cleaning up GitLab project(s).
```

Example: **Dry run to cleanup pipelines before 1st January 2020 from specific branches.**

Command:

```bash
export GITLAB_ACCESS_TOKEN="your-pat-project-group-token" # e.g. "gpat-123abc"
PROJECT_ID=123
DATE="2020-01-01"

python3 gitlab_cleanup_projects.py --project $PROJECT_ID --cleanup pipelines --date $DATE --branches main dev --dry-run

# Output/Log:
# Authenticated successfully with https://gitlabstg.cern.ch
# Running in Dry Run mode. No actual deletions will be performed.
# Fetching GitLab project(s)...
# Found 1 project(s) for cleanup. Starting cleanup operations...
# -- Scanning project susuara/playground-project-1 (ID: 910) (URL: https://gitlabstg.cern.ch/susuara/playground-project-1) for cleanup...
# ...
# Checking pipeline #99999 for branch 'main' created on 2019-11-11 | Total no. of pipelines scanned: 1
# Pipeline #99999 for branch 'main' created on 2019-11-11 is older than 2020-01-01 and is marked for deletion. Pipeline URL: https://gitlabstg.cern.ch/susuara/playground-project-1/-/pipelines/99999
# ...
# Finished cleaning up GitLab project(s).
```

Example: **Dry run to cleanup pipelines before 1st January 2020 excluding specific branches.**

Command:

```bash
export GITLAB_ACCESS_TOKEN="your-pat-project-group-token" # e.g. "gpat-123abc"
PROJECT_ID=123
DATE="2020-01-01"

python3 gitlab_cleanup_projects.py --project $PROJECT_ID --cleanup pipelines --date $DATE --exclude-branches main --dry-run

# Output/Log:
# Authenticated successfully with https://gitlabstg.cern.ch
# Running in Dry Run mode. No actual deletions will be performed.
# Fetching GitLab project(s)...
# Found 1 project(s) for cleanup. Starting cleanup operations...
# -- Scanning project susuara/playground-project-1 (ID: 910) (URL: https://gitlabstg.cern.ch/susuara/playground-project-1) for cleanup...
# ...
# Checking pipeline #99998 for branch 'dev' created on 2019-11-11 | Total no. of pipelines scanned: 1
# Pipeline #99998 for branch 'dev' created on 2019-11-11 is older than 2020-01-01 and is marked for deletion. Pipeline URL: [<URL>](https://gitlabstg.cern.ch/susuara/playground-project-1/-/pipelines/99998)
# ...
# Finished cleaning up GitLab project(s).
```
