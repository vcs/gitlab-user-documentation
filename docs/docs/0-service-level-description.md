---
title: Git Service - Service Level Description
slug: /
---

## Summary

The Git service is currently running GitLab, a self-service code hosting application based on Git that provides collaboration and code review features (similar to GitHub), introduced in spring 2015. In addition to repository hosting, GitLab offers GitHub-like code review and collaborative features (in particular a merge requests workflow). This is a fully self-service platform and is the recommended place for all new projects. Please read [our guide to get started with GitLab](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137). GitLab also offers:

- A built-in Continuous Integration engine to facilitate automatic test, build and deployment of software projects. See [our article about GitLab-CI](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003690).
- A built-in Docker container and package registry available in **gitlab-registry.cern.ch** Detailed information is provided in [this article](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0004284).
- Owners of SVN repositories are encouraged to move them to GitLab. See our article on [migrating projects from SVN](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0001905).

:::info

For all GitLab features, it is necessary to visit [https://gitlab.cern.ch](https://gitlab.cern.ch) in a web browser at least once with each account that needs access to GitLab, in order to initialize this account.

:::

## Homepage

The hosting platform for Git repositories is currently located at [https://gitlab.cern.ch](https://gitlab.cern.ch).

## Service Managers

Ismael Posada Trobo (ismael.posada.trobo@cern.ch)

IT Department - PW-WA section

## General Description

### GitLab license at CERN

Since May 2023, GitLab at CERN has adopted the [GitLab Enterprise Edition - Ultimate](https://about.gitlab.com/pricing/ultimate/) tier. This Ultimate tier unlocks a powerful suite of [advanced features](https://about.gitlab.com/features/) and [feature flags](https://docs.gitlab.com/ee/user/feature_flags.html) designed to streamline our development process and enhance collaboration, as well as to empower security and compliance frameworks.

Advanced features and feature flags, available as part of the self-managed installation and Ultimate tier, come with varying default settings (enabled or disabled) and are provided as is. I.e, those coming enabled by default will stay enabled, and those coming disabled by default will stay disabled.

:::info

Please note that for the safety and optimal performance of GitLab at CERN, only features that raise privacy concerns or disrupt normal operations will be disabled without further notice.

:::

Whether our community require a specific feature to be enabled or disabled, the GitLab Team encourages our users to [submit a request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service), to assess the feasibility and the impact of the requested feature in our systems and in the projects/groups before implementation.

### Roadmap

Planning, releases and high level features can be found under our [Roadmap](./99-roadmap.md)

### Data backups

Data backups can be classified in two:

- GitLab CephFS backups (git data).
- Non-git data backups.

#### GitLab CephFS Backups

Gitlab data is currently stored on 4 CephFS shares provided by the Ceph Service. Backup of this areas is performed by the Ceph Team (IT-SD):

```bash
/cephfs-levinson/volumes/_nogroup/2df13044-180f-43b0-9d17-7361c1857448/03051040-19ee-4c81-9856-eeeaf95f2841
/cephfs-levinson/volumes/_nogroup/44093fe3-a050-4573-9f34-4d50f1e14066/5ef27bba-7b37-4e02-8e1d-9966eac961c2
/cephfs-levinson/volumes/_nogroup/c47fa02b-277c-4086-9357-868a65f696e4/da0e9066-5620-441f-8fa7-5ee3ef8da23d
/cephfs-levinson/volumes/_nogroup/82517cc4-ccdf-4ab9-b473-ca41e416284e/e9e9ce96-fca4-4b39-9608-9b6dd31d6375
```

##### Backup frequency and retention policy

- All four shares are backed-up twice per day, at ~07:00 and ~19:00.
- A retention policy (purge of old backups) is applied like the following:
  - Daily backups up to two months back (2 daily backups for the last month).
  - Weekly backups up to 6 months back.
  - Monthly backups up to 12 months back.

![Backup frequency and retention policy](/img/assets/sld/cephfs-backup-retention-policy.png)

##### Backup features

- Backup data is stored in a different geographical location (Prevessin, FR) than the source data.
- Backup data is encrypted using AES-256.

##### Backup consistency

- Each cephfs volume is backed-up independently. A time skew between backups of each volume is to be expected.
- Backup is performed from a live file system, hence point-in-time consistency cannot be guaranteed.
- The GitLab team is responsible for regularly exercising and verifying that backups are usable.

#### Non-git data

Non-git data (Docker images, user uploads, job artifacts) is stored in [S3](https://cern.service-now.com/service-portal?id=functional_element&name=S3ObjectStorage). S3 versioning is configured so that deleted items are kept for 1 month before being removed from storage. In addition to S3's own internal redundancy and data protection mechanisms, a replica of the GitLab content stored in S3 is kept on a separate storage system (Ceph RBD volumes) and updated daily.

## Service Documentation

Documentation is available under [https://gitlab.docs.cern.ch](https://gitlab.docs.cern.ch).

- GitLab-specific documentation is under the [GitLab topic](https://cern.service-now.com/service-portal?id=cern_kb_search&spa=1&query=Gitlab) (in particular our [Getting started](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137) guide) and in [GitLab's help page](https://gitlab.cern.ch/help).
- General help about Git usage and best practices can be found in the "Further guides" section on this page and under the topic ["Using Git"](https://cern.service-now.com/service-portal/?id=cern_kb_search&spa=1&query=Using%20Git).

## Service Level

- Use of the Git service is granted to **CERN account holders** inside and outside CERN. Users without a CERN account cannot login to gitlab.cern.ch. Public projects with contributors from the general public or projects requiring collaboration with persons or companies not entitled to a CERN account may consider public Git repository hosting platforms such as GitHub or gitlab.com. For more details see [KB0003132](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003132).
- The Git service aims at providing version control for **​text and source code**​. Storage of large binary files in general is outside the scope of the service. As a number of [options exist already at CERN](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003887), Git-LFS ([Large File Storage](https://gitlab.cern.ch/help/topics/git/index.md#git-large-file-storage-lfs)) is only provided with a restrictive quota (10GB per GitLab project as of May 2018) to help with migration from SVN.
- Service availability is indicated on the availability displays available from the portal or this page.
- Like most other IT applications, Git and Gitlab service hours are regular CERN working hours, e.g. weekdays from 8:30 to 17:30. Although the services are designed to be reliable and downtime is not expected, any service releated intervention outside these hours are only on a "best effort" basis.
- GitLab Projects hosted in a user's personal workspace are archived after the user leaves CERN. GitLab Groups are however persistent and ownership can be transferred to other persons as necessary. Guidelines can be found in the ["Getting Started" article](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137).
- Any project or users that are found to abuse the service or not respecting the [CERN Computing rules](https://security.web.cern.ch/rules/en/index.shtml), may be removed upon notification of the administrators of that repository.


## User Support

User support for the GitLab Service is provided via the Service Portal. If you have questions and requests, please open a [support ticket](https://cern.service-now.com/service-portal?id=service_element&name=git-service).

Support tickets will be escalated to our small team of Service Supporters. Although we endeaver to respond to all user requests, the huge number of hosted projects / repositories (close to 200'000) means that we need focus on questions or requests concerning the infrastructure and the service itself. We are however unable to provide help specific to particular projects, for example related to custom CI/CD pipelines, software developement lifecycle, external tools to manage GitLab project etc.  

Before requesting help, please ensure that:

* You have followed the [CERN GitLab Service documentation](https://gitlab.docs.cern.ch/docs) as well as the [official GitLab docs](https://docs.gitlab.com/user/), which include details, examples, frequently asked questions and configuration instructions.
* You have got as far as possible and have a well defined issue that needs expert input.
* If you seem to have found a bug in GitLab software, please [follow our recommendations](https://gitlab.docs.cern.ch/docs/contribute-to-giltab#did-you-found-a-bug).

## Feedback

To send us your suggestion, or to make a feature request please go to the [Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
