---
title: GitLab Duo Chat
---

:::info

The GitLab Duo Chat will only be enabled for the GitLab Duo pilot participants (see [OTG0152725](https://cern.service-now.com/service-portal?id=outage&n=OTG0152725)).

:::

The GitLab Duo Chat is our personal AI-powered assistant. For those familiar with this kind of tools, it is the ChatGPT-like version for GitLab.

It can be used directly from the GitLab UI (top-right), or to be integrated in our preferred Web IDE.

Visit [GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat/) for a more general information.

## Ask GitLab Duo Chat

For examples about how to get the most out of the GitLab Duo Chat, please visit the [Ask GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html) documentation.

## GitLab Duo Chat best practices

For best practices when using GitLab Duo Chat, please visit [GitLab Duo Chat best practices](https://docs.gitlab.com/ee/user/gitlab_duo_chat/best_practices.html)

Do not hesitate to see examples of usage visiting the [Youtube - GitLab Duo Coffee Chat](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ) videos with plenty of examples.
