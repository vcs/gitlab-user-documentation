---
title: GitLab Duo
---

:::info

The GitLab Duo AI-powered suite will become available for a selected group of users until end of December 2024.

:::

GitLab Duo is a suite of AI-powered features that is under study at CERN to be integrated into GitLab's DevSecOps Platform. Its purpose is to assist developers and teams across the entire software development lifecycle, acting as an intelligent, always-on agent that monitors, optimizes, and secures projects, allowing developers to focus on innovation. It also helps with planning refinement, security risk resolution, CI/CD pipeline health, and analytics charting, aiming at increasing velocity and address pain points throughout the development process.

Some AI-Powered capabilities and key aspects of GitLab Duo are:

* GitLab Duo suggests code, explains vulnerabilities, forecasts value streams, and more. See further information under [GitLab Duo features](./2-GitLab%20Duo%20features.md)
* GitLab Duo Chat helps you write and understand code faster, quickly answering questions in a chat window. See further information under [GitLab Duo Chat](./1-GitLab%20Duo%20Chat.md)

For further information about the list of features offered by the GitLab Duo AI-powered suite, please navigate to the [generally available features](https://docs.gitlab.com/ee/user/gitlab_duo/#generally-available-features) link.

## General limitations

AI tools are very useful and have the potential to make us much more productive, but they should be used with caution. Using AI tools may have implications in developers workflows, that's why it is essential to approach them with a critical and informed mindset.

Here are some general implications to keep in mind:

* **Do not blindly trust AI tools**: They are generally trained on big datasets, but they still can "hallucinate", make mistakes, or produce inaccurate results. **Always verify their output and suggestions.**
* **Use AI tools as a tool, not as a replacement**: AI tools can be a very valuable tools that may improve developers capabilities, but they should never replace human expertise or verification.
* **Always follow best practices**: Ensure that best practices for testing, code reviews, software security etc. are followed to maximize the benefits of the AI tools and to minimize risks.

## Privacy implications

Using external services such as GitLab Duo may have privacy implications for our users' data. That's why the Git Service take this into consideration very carefully during our selection process, ensuring that user data is protected and aligns with our strict privacy standards, as well as that the external processor complies with the principles set out in the [Operational Circular 11 - OC 11](https://cds.cern.ch/record/2651311/files/Circ_Op_Angl_No11_Rev0.pdf).

:::warning

When using services such as GitLab Duo, GitLab Duo chat, etc., the Git Service encourage our users to refrain from sharing any personal information in the prompts. This includes things like names, addresses, phone numbers, or any other details that could be used to identify you or another person.

:::

For further information about the Git Service privacy notice, visit the [Privacy Notice: Git Service](https://cern.service-now.com/service-portal?id=privacy_policy&se=git-service&notice=git-service).

## Pilot phase

We are internally evaluating the adoption of the GitLab Duo suite at CERN, as [expressed in the last IT Users Meeting (ITUM)](https://indico.cern.ch/event/1397307/contributions/5873112/attachments/2887690/5061458/ITUM-42,%20Technical%20Service%20Updates.pdf), held in July 2024, to see what is the interest of the CERN community and benefits it produces when using these AI capabilities.

The pilot will run as of October 2024 (see [OTG0152725](https://cern.service-now.com/service-portal?id=outage&n=OTG0152725)).
