---
title: GitLab Duo Features
---

GitLab Duo is a set of AI-powered tools designed to enhance productivity and streamline various tasks within the software development lifecycle. These features are integrated into GitLab’s UI and IDE extensions to help you work more efficiently. Below is an overview of the key features available in GitLab Duo.

[![GitLab Duo Chat](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fplayer.vimeo.com%2Fvideo%2F933806295%3Fautoplay%3D1%26loop%3D1%26autopause%3D0%26background%3D1%26muted%3D1)](https://player.vimeo.com/video/933806295?autoplay=1&loop=1&autopause=0&background=1&muted=1)

## GitLab Duo Chat

**Description**: GitLab Duo Chat helps users write and understand code faster by providing instant responses to queries about GitLab, codebase, and projects. It allows users to ask questions in a chat interface and receive AI-powered answers to speed up workflows.

- [Watch overview](https://www.youtube.com/watch?v=ZQBAuf-CTAY&list=PLFGfElNsQthYDx0A_FaNNfUm9NHsK6zED)
- [View documentation](https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html)

## Discussion Summary

**Description**: This feature summarizes lengthy discussions in issues, making it easier for team members to quickly understand the key points of a conversation, even if they join midway through. It extracts and presents the most relevant information from complex discussions.

- [Watch overview](https://www.youtube.com/watch?v=IcdxLfTIUgc)
- [View documentation](https://docs.gitlab.com/ee/user/discussions/index.html#summarize-issue-discussions-with-duo-chat)

## Code Suggestions

**Description**: Code Suggestions assists developers by generating code snippets and providing suggestions as they type. It helps speed up coding by recommending common patterns or completing code based on the context.

- [Watch overview](https://youtu.be/ds7SG1wgcVM)
- [View documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html)

## Code Explanation

**Description**: Code Explanation helps users understand complex code by providing clear, concise explanations. This is especially useful when reviewing unfamiliar code or learning a new codebase.

- View documentation for explaining code in:
  - [The IDE.](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#explain-selected-code)
  - [A file.](https://docs.gitlab.com/ee/user/project/repository/code_explain.html)
  - [A merge request.](https://docs.gitlab.com/ee/user/project/merge_requests/changes.html#explain-code-in-a-merge-request)

## Test Generation

**Description**: Test Generation helps developers write tests for their code automatically. By selecting a portion of code, the AI can generate tests that validate its functionality, ensuring bugs are caught early in the development process.

- [Watch overview](https://www.youtube.com/watch?v=zWhwuixUkYU&list=PLFGfElNsQthYDx0A_FaNNfUm9NHsK6zED)
- [View documentation](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#write-tests-in-the-ide)

## Merge Commit Message Generation

**Description**: This feature generates meaningful commit messages when merging code. It ensures that commit messages follow best practices and clearly describe the changes, making it easier to understand the code history.

- [View documentation](https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#generate-a-merge-commit-message)

## Root Cause Analysis

**Description**: Root Cause Analysis helps developers diagnose CI/CD pipeline failures by analyzing job logs and pinpointing the source of the problem. It provides insights into why a job failed and suggests potential fixes.

- [Watch overview](https://www.youtube.com/watch?v=MLjhVbMjFAY&list=PLFGfElNsQthZGazU1ZdfDpegu0HflunXW)
- [View documentation](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#troubleshoot-failed-cicd-jobs-with-root-cause-analysis)

## Vulnerability Explanation

**Description**: Vulnerability Explanation provides detailed descriptions of vulnerabilities found in the code, explaining how they might be exploited and offering suggestions on how to fix them.

- [Watch overview](https://www.youtube.com/watch?v=MMVFvGrmMzw&list=PLFGfElNsQthZGazU1ZdfDpegu0HflunXW)
- [View documentation](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability)

## AI Impact Dashboard

**Description**: The AI Impact Dashboard helps teams measure how AI features are improving their productivity and software development lifecycle (SDLC) metrics. It provides visualizations of AI-driven improvements, compares team performance, and tracks AI adoption over time.

- [View documentation](https://docs.gitlab.com/ee/user/analytics/ai_impact_analytics.html)

## GitLab Duo for the CLI

**Description**: GitLab Duo provides support in the command-line interface through the `glab duo ask` command. This feature helps users discover or recall Git commands quickly, without needing to consult external documentation.

- [View documentation](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html#gitlab-duo-for-the-cli)

For more information and further documentation on all features, visit the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/gitlab_duo/).
