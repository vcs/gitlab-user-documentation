---
title: Custom roles and permissions
---

GitLab admins have the ability to create custom roles that are accessible from projects and groups in the GitLab instance. Those custom roles are useful in case you want to enhance a user's permissions without giving them a higher role. This is practical when a user with a specific role is needed to perform actions that are restricted for his/hers pre-defined role.

## How to assign a custom role?

The custom roles are now listed in the drop down menu under the main roles.
you can assign a custom role to a user:

- When you invite a new user to a group or project by clicking on **Manage** > **Members** > **Invite Users**.
  ![Custom Roles](/img/assets/user-account-options/custom_roles.png)
- By altering an existing user's role by clicking on **Manage** > **Members** in a group or project.
- By setting an ldap synchronization to a group and specifying the proper LDAP access.

## How to request a custom role?

The gitlab admins can assist you on creating your custom role via the admin panel.

To do this please [open a ticket](https://cern.service-now.com/service-portal?id=service_element&name=git-service) with us and:

1. Provide the preferred **Name** of the custom role you want to name.
2. Choose the **Base Role** from the [role list](https://docs.gitlab.com/ee/user/permissions.html#roles)
3. Choose the **Permissions** you want to add from this [abilities list](https://docs.gitlab.com/ee/user/custom_roles/abilities.html)

Once you provide the above information we will handle the creation of the custom role.
