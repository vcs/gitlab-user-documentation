---
title: Security Policies
---

Policies in GitLab provide security teams with a way to require scans of their choice to be run according to the specified configuration. This can be when a project pipeline runs, as per a particular schedule or according to a given set of rules. GitLab supports the following types of policies:

1. [Scan Execution Policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html) - these policies can be used to run security scans on a specified schedule or with the project pipeline. For example, running a scan every day or whenever a branch is merged into main/master. 
2. [Scan Result Policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html) - these policies can be used to take action based on scan results. For example, preventing an MR from being merged if one or more critical vulnerabilities are detected. 

All security policies are stored as YAML in a separate security policy project that gets linked to a development project, group, or sub-group. ***This association is a one-to-many relationship. Which means that while one policy can be imported by many groups/projects, one group/project can import only one policy.***

:::info

If a policy is imported at the group level, all the projects and sub-groups in that group automatically inherit it. **This method is preferred over importing policies at the project level because only group owners have permissions to modify/unlink the policies.**

:::

*The Computer Security and GitLab teams have specified a set of standard Scan Execution Policies. The most suitable one from these must be imported in each group/project. These security policy projects can be found in the **[CERN-SecurityPolicies](https://gitlab.cern.ch/cert-security-policies)** group.*

**Currently users are limited to import only one policy per project (by design in GitLab).** However if you wish to have more control on policy configuration or require additional policies, you can define your own policy projects. This can be done easily using the [Policy Editor](https://docs.gitlab.com/ee/user/application_security/policies/#policy-editor) provided by GitLab.

## Policy projects provided by CERN-SecurityPolicies

1. [**Scheduled SEP - SAST - Secret Detection**](https://gitlab.cern.ch/cert-security-policies/cert-security-policies-security-policy-project): 
    This project defines a Scheduled Scan Execution Policy (SEP) that runs Static Application Security Testing and Secret Detection scans default branches.

## Security Policy Bot

Security bot users will be automatically created in GitLab to support managing background tasks, and to enforce security policies for *all newly created or updated security policy project links*. This will also make it much clearer for users within an enforced project when pipelines are executed on behalf of a security policy, as this bot user will be the pipeline author.

![Policy Bot](/img/assets/secure-your-application/security-policies/policy_bot.png)

When a security policy project is linked to a group or subgroup, a security policy bot will be created in each project in the group or subgroup. When a link is made to a group, subgroup, or an individual project, a security bot user will be created for the given project or for any projects in the group or subgroup. 

## Improvements scheduled for upcoming GitLab versions

1. Allow users to define branch exceptions to enforced security policies - With branch exceptions, you can more granularly enforce policies and exclude enforcement for any given branch that is out of scope.

## Reference

1. GitLab documentation page on *Security Policies* - [Policies | Gitlab](https://docs.gitlab.com/ee/user/application_security/policies/)
2. Upcoming GitLab features - [Release Overview](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features&selectedCategories=API+Security&selectedCategories=Secret+Detection&selectedCategories=Security+Policy+Management&selectedCategories=SAST&minVersion=16_04)
