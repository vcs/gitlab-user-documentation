# CIS GitLab Benchmark Scanner

The [CIS GitLab Benchmark Scanner](https://gitlab.com/gitlab-org/govern/compliance/engineering/cis/gitlabcis) is a Python-based CLI tool designed to audit GitLab projects for compliance against the [CIS GitLab Benchmark](https://about.gitlab.com/blog/2024/04/17/gitlab-introduces-new-cis-benchmark-for-improved-security/). It provides a comprehensive set of over 125 recommended security guidelines that cover critical aspects like access control, authentication, data protection, and secure development practices etc. For more information, refer to the [CIS GitLab Benchmark documentation](https://gitlab.com/gitlab-org/govern/compliance/engineering/cis/gitlabcis/-/tree/main/docs).

## Pre-requisite & Setup of Environment

You need the following to run the tool:

- A GitLab Personal Access Token (PAT) with at least the `read_api` scope.
- [Docker](https://docs.docker.com/get-started/get-docker/) installed on your machine.

Run the following command to setup the environment as per your operating system:

For Linux:

```bash
docker run -it --rm \
  -v "$(pwd)":/gitlabcis \
  -w /gitlabcis \
  python:3.10 sh -c "
    pip install requests gitlabcis &&
    sed -i 's/%Y-%m-%dT%H:%M:%S\\.\\%fZ/%Y-%m-%dT%H:%M:%S\\.\\%f%z/g' /usr/local/lib/python*/site-packages/gitlabcis/benchmarks/source_code_1/repository_management_1_2.py &&
    bash
  "
```

For PowerShell:

```powershell
docker run -it --rm -v ${PWD}:/gitlabcis -w /gitlabcis python:3.10 sh -c "pip install requests gitlabcis && sed -i 's/%Y-%m-%dT%H:%M:%S\\.\\%fZ/%Y-%m-%dT%H:%M:%S\\.\\%f%z/g' /usr/local/lib/python*/site-packages/gitlabcis/benchmarks/source_code_1/repository_management_1_2.py && bash"
```

The command above includes a patch fix for time format issue in the `benchmarks/source_code_1/repository_management_1_2.py` file.

## Notes

Example command: `gitlabcis https://gitlab.cern.ch/<Project Full Path>> --token <PAT with read_api scope>`

Note that by default, the tool does a full scan of all the benchmarks, benchmarks can be skipped or cherry picked with CLI options:

```bash
-ci [CIS_CONTROL_IDS ...], --cis-controls [CIS_CONTROL_IDS ...]
                        The IDs of the CIS Controls to audit (e.g. 18.1)
-ids [RECOMMENDATION_IDS ...], --recommendation-ids [RECOMMENDATION_IDS ...]
                        The IDs of the recommedation controls to audit (e.g. 1.1.1)
-s [RECOMMENDATION_IDS_TO_SKIP ...], --skip [RECOMMENDATION_IDS_TO_SKIP ...]
                        The IDs of the recommedation controls to SKIP (e.g. 1.1.1)
-p PROFILE, --profile PROFILE
                        Which benchmark profile to use (default: both 1 & 2)
-g [IMPLEMENTATION_GROUPS ...], --implementation-groups [IMPLEMENTATION_GROUPS ...]
                        Which CIS Implementation Group to use ['IG1', 'IG2', 'IG3'] (default: all)
```

Run `gitlabcis --help` for more information.
