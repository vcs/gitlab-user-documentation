---
title: Pipeline Execution Policies
---

GitLab Pipeline Execution Policies empowers you to define and enforce standardized pipeline configurations across multiple projects and groups. This feature is part of GitLab's advanced security and compliance offerings, available with Ultimate licenses. Pipeline Execution Policies enhance security and compliance by providing the following capabilities:

* **Centralized Management:** Manage pipeline configurations from a central location, ensuring consistency across your group and projects.
* **Enforced Compliance and Security Checks:**  Guarantee that critical security and compliance checks are executed in every pipeline.
* **Apply GitLab Security Templates:** Easily integrate pre-defined security templates into your pipelines for common security best practices.
* **Execute Custom Scripts:** Implement custom scripts for specific security or compliance requirements.
* **Execute Reporting Rules through CI Jobs:** Generate reports based on pipeline execution to monitor compliance and identify potential issues.

## Types of Pipeline Execution Policies

Pipeline Execution Policies offer two primary modes:

1. **Inject:** Inject a predefined pipeline configuration into existing project pipelines. This allows you to add mandatory stages or jobs without modifying individual project `.gitlab-ci.yml` files. This is useful for adding security scans, license checks, or other essential tasks.

    ![Inject policy type](/img/assets/secure-your-application/pipeline-exec-policies/inject-policy.png)

2. **Override:** Override existing pipeline configurations in projects. This allows you to enforce a specific pipeline structure or configuration, ensuring compliance with organizational standards. This can be used to restrict certain actions or enforce specific build processes.

    ![Override policy type](/img/assets/secure-your-application/pipeline-exec-policies/override-policy.png)

    🟩 Green color: Project's Pipeline job,
    🟥 Red color: Pipeline execution policy job

## How to Enable a Pipeline Execution Policy

Enabling a Pipeline Execution Policy involves the following steps:

1. **Navigate to the "Secure" Menu.** In your GitLab group or project, go to the "Secure" section on the side bar menu.
2. **Click on "Policies" and then "New Policy"**
3. **Choose the "Pipeline Execution Policy" type**
4. **Configure the Policy:** Define the policy's name, description, policy type (Inject or Override), the project that hosts the policy file and the filepath of it.
5. **Click on "Update via merge request" button.** This will create a merge request to a new repository in the group that will host the policy configuration.
6. **Finally click on the merging button**

## Example Pipeline policy

```yaml
# Example of a pipeline execution policy that runs:
# - GitLab's Secret Detection scan in the test stage
# - and a custom script at the .post stage
stages:
- test
- .post

include:
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

post_exec:
  stage: .post
  script:
    - echo "Post execution job"
```

## Important Considerations

**Policy Scope**: Policies can be applied at the group or project level. Group-level policies apply to all projects, subgroups and subprojects within the group. Currently you cannot use policies from a project outside your group.

## References

[GitLab Pipeline Execution Policies Documentation](https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html)
