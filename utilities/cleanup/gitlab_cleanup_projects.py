# Author: Git Service
# Contact: https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service
# Description: This script is used to cleanup GitLab artifacts and container images based on the provided date threshold.
# Usage: Run python gitlab_cleanup_artifact_registry.py --help for usage instructions.


import sys
import argparse
import logging
import os
import math
import json
import time
from datetime import datetime
import gitlab
import requests


# Global Variables
# -----------------------------------------------------------------------------------------------------------------------------
# GitLab API base URL, should be set as an environment variable or directly in the script
GITLAB_API_URL = os.getenv('GITLAB_API_URL', 'https://gitlab.cern.ch')
# GitLab access token for authentication, can be set as an environment variable or passed as an argument
GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN', '')
# Log file name and location, can be set directly in the script only
LOG_FILE = os.getenv('LOG_FILE', 'gitlab_cleanup')
# Dry run flag
DRY_RUN = False
# Logger object
LOGGER = None
# Minimum access level required to access the project. 10 = Guest, 20 = Reporter, 30 = Developer, 40 = Maintainer, 50 = Owner.
MIN_ACCESS_LEVEL = 30
# -----------------------------------------------------------------------------------------------------------------------------


def setup_logging(log_file_name):
    """
    Set up logging configuration to log to both file and stdout.

    Returns:
        logging.Logger: Configured logger object.
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    stream_formatter = logging.Formatter('%(message)s')

    # File handler for logging
    file_handler = logging.FileHandler(log_file_name)
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    # Stream handler for stdout
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(stream_formatter)
    logger.addHandler(stream_handler)

    return logger


def setup_gitlab_client(token, url):
    """
    Set up the GitLab client using python-gitlab library.

    Parameters:
        token (str): The GitLab access token for authentication.
        url (str): The GitLab API URL.

    Returns:
        gitlab.Gitlab: Authenticated GitLab client.
    """
    try:
        gitlab_client = gitlab.Gitlab(url, private_token=token)
        gitlab_client.auth()

        LOGGER.info(f"Authenticated successfully with {url}") 

        return gitlab_client
    
    except gitlab.exceptions.GitlabAuthenticationError:
        LOGGER.error(f"Failed to authenticate with {url}. Check your token, scopes and try again.")
        sys.exit(1)


def initialize_script_parameters(args):
    """
    Initialize the script parameters and set up the GitLab client.

    Parameters:
        args (argparse.Namespace): Parsed CLI arguments.

    Returns:
        gitlab.Gitlab: Authenticated GitLab client.
    """
    global DRY_RUN, GITLAB_ACCESS_TOKEN, LOGGER
    
    # Set the dry run flag
    DRY_RUN = args.dry_run

    # Set up logging
    LOGGER = setup_logging(f"{LOG_FILE}{'_dry-run' if DRY_RUN else ''}_{args.cleanup}_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log")

    # Determine the GitLab Access Token from argument, environment, or script
    GITLAB_ACCESS_TOKEN = GITLAB_ACCESS_TOKEN or args.token or os.getenv('GITLAB_PRIVATE_TOKEN', GITLAB_ACCESS_TOKEN)
    if not GITLAB_ACCESS_TOKEN:
        LOGGER.error("GitLab Access Token must be passed as an argument, set as GITLAB_ACCESS_TOKEN environment variable, or directly in the script.")
        sys.exit(1)

    # Check for project id or json file in arguments
    if args.json and not args.project:
        LOGGER.error("Please specify --project ID when using --json option.")
        sys.exit(1)

    # Set up GitLab client
    gitlab_client = setup_gitlab_client(GITLAB_ACCESS_TOKEN, GITLAB_API_URL)

    return gitlab_client


def parse_date(date_str):
    """
    Convert provided date string into a datetime object.

    Parameters:
        date_str (str): Date in YYYY-MM-DD format.

    Returns:
        datetime: Parsed datetime object.
    """
    try:
        return datetime.strptime(date_str, '%Y-%m-%d').replace(tzinfo=None)
    
    except ValueError:
        LOGGER.error("Incorrect date format, should be YYYY-MM-DD")
        sys.exit(1)


def convert_size_to_human_readable(size_bytes):
    """
    Convert bytes into a human-readable format (e.g., KB, MB, GB).

    Parameters:
        size_bytes (int): Size in bytes.

    Returns:
        str: Human-readable size string.
    """
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return f"{s} {size_name[i]}"


def retrieve_and_cleanup_projects(gitlab_client, resource, threshold_before_date, branches=None, exclude_branches=None):
    """
    Retrieve project(s) and perform cleanup operations based on the provided resource type (artifacts, images, pipelines, all) and date threshold.

    Parameters:
        gitlab_client (gitlab.Gitlab): Authenticated GitLab client.
        resource (str): Resource type to clean up ('artifacts', 'images', 'pipelines', 'all').
        threshold_before_date (datetime): Date threshold to delete job artifacts, images or pipelines before.
    """

    if DRY_RUN:
        LOGGER.info("Running in Dry Run mode. No actual deletions will be performed.")

    LOGGER.info(f"Fetching GitLab project(s)...")
    
    # Uncomment later to provide the option to cleanup all projects
    # if args.project == 'all':
    #     projects = get_all_projects_with_min_access_level(gitlab_client, MIN_ACCESS_LEVEL)
    # else:
    #     projects = [get_project_by_id(gitlab_client, args.project)]

    projects = [get_project_by_id(gitlab_client, args.project)]

    if not projects:
        LOGGER.error("No GitLab project(s) found with the provided project id or access token. Exiting...")
        return

    # Perform cleanup operations based on the provided resource type
    LOGGER.info(f"Found {len(projects)} project(s) for cleanup. Starting cleanup operations...")
    for project in projects:
        # Separate logging for each project
        LOGGER.info(f"-- Scanning project {project.path_with_namespace} (ID: {project.id}) (URL: {project.web_url}) for cleanup...")

        if resource in ['artifacts', 'all']:
            delete_job_artifacts(project, threshold_before_date)

        if resource in ['images', 'all']:
            delete_container_images(project, threshold_before_date)
        
        if resource in ['pipelines', 'all']:
            delete_pipelines(project, threshold_before_date, branches, exclude_branches)
    
    LOGGER.info("Finished cleaning up GitLab project(s).")


def cleanup_project_from_json(json_file_path, project_id, resource, threshold_before_date):
    """
    Cleanup project(s) based on job or pipeline data from a provided JSON file, filtering by date.

    Parameters:
        json_file_path (str): Path to the JSON file containing job/pipeline data.
        resource (str): Resource type to clean up ('artifacts', 'images', 'pipelines', 'all').
        threshold_before_date (datetime): Date threshold to job delete artifacts, images or pipelines before.
    """
    if DRY_RUN:
        LOGGER.info("Running in Dry Run mode. No actual deletions will be performed.")

    LOGGER.info(f"-- Scanning JSON file {json_file_path} with project ID {project_id} for cleanup...")

    if resource in ['artifacts']:
        delete_job_artifacts_from_json(json_file_path, project_id, threshold_before_date)

    if resource in ['images']:
        LOGGER.error("Image cleanup based on JSON file is not supported yet. Please consider using the project ID instead.")
        return

    if resource in ['pipelines']:
        delete_pipelines_from_json(json_file_path, project_id, threshold_before_date)
    
    LOGGER.info("Finished cleaning up GitLab project(s).")


def delete_job_artifacts(project, threshold_before_date):
    """
    Delete job artifacts for a given project that are older than the provided date threshold.

    Parameters:
        project (gitlab.v4.objects.Project): The GitLab project object.
        threshold_before_date (datetime): Date threshold to delete job artifacts before.
    """
    # List to store jobs marked for deletion
    jobs_marked_delete_artifacts = []

    try:
        # Jobs are only allowed to be fetched with order_by id and sort desc (latest first). Might change in future.
        # https://docs.gitlab.com/ee/api/rest/index.html#keyset-based-pagination
        jobs = project.jobs.list(pagination="keyset", order_by="id", sort="desc", per_page=100, iterator=True)
    except gitlab.exceptions.GitlabListError as e:        
        # Skip project if permissions are insufficient
        if e.response_code == 403:
            LOGGER.error(f"Insufficient permissions to fetch jobs for project {project.name} (ID: {project.id}). Skipping project...")
            return
        
        LOGGER.error(f"An error occurred while fetching jobs for project {project.name} (ID: {project.id}): {e}")
        return
    
    counter_job = 0
    counter_job_with_artifacts = 0

    for job in jobs:
        counter_job += 1

        # Skip jobs without artifacts
        artifacts = job.attributes['artifacts']
        if not artifacts:
            continue

        # Skip jobs with only job.log artifact
        if len(artifacts) == 1 and artifacts[0]['file_type'] == 'trace' and artifacts[0]['filename'] == 'job.log':
            continue

        created_at = datetime.fromisoformat(job.created_at).replace(tzinfo=None)

        counter_job_with_artifacts += 1
        LOGGER.info(f"Checking job #{job.id} created on {created_at.strftime('%Y-%m-%d')} | Total no. of jobs scanned: {counter_job} | Total no. of jobs found with artifacts: {counter_job_with_artifacts}")

        # Mark job for deletion if older than before_date
        if created_at < threshold_before_date:
            jobs_marked_delete_artifacts.append(job)
            LOGGER.info(f"Job #{job.id} created on {created_at.strftime('%Y-%m-%d')} is older than {threshold_before_date.strftime('%Y-%m-%d')} and its artifacts are marked for deletion. Job URL: {job.web_url}")

    if not jobs_marked_delete_artifacts:
        LOGGER.info(f"No jobs with artifacts older than {threshold_before_date.strftime('%Y-%m-%d')} found in project {project.path_with_namespace}")
        return

    if jobs_marked_delete_artifacts:
        LOGGER.info(f"Deleting artifacts for {len(jobs_marked_delete_artifacts)} jobs in project {project.path_with_namespace}...")
        for job in jobs_marked_delete_artifacts:
            if DRY_RUN:
                LOGGER.info(f"DRY RUN: Would delete artifacts for Job #{job.id}")
                continue
            
            job.delete_artifacts()
            LOGGER.info(f"Deleted artifacts for Job #{job.id}")

            # Adding a sleep to avoid hitting rate limits
            time.sleep(0.1)


def delete_job_artifacts_by_id(job_id, project_id, access_token=GITLAB_ACCESS_TOKEN):
    """
    Delete job artifacts using the job ID.

    Parameters:
        job_id (int): The ID of the job whose artifacts should be deleted.
        access_token (str): GitLab access token for authentication.
    """
    url = f"{GITLAB_API_URL}/api/v4/projects/{project_id}/jobs/{job_id}/artifacts"
    
    headers = {
        'PRIVATE-TOKEN': access_token
    }

    if DRY_RUN:
        LOGGER.info(f"DRY RUN: Would delete artifacts for Job #{job_id}")
        return

    try:
        response = requests.delete(url, headers=headers)
        response.raise_for_status()  # Raise an exception for HTTP errors (4xx and 5xx)
        if response.status_code == 204:
            LOGGER.info(f"Deleted artifacts for Job #{job_id}")
        else:
            LOGGER.error(f"Unexpected response for Job #{job_id}: {response.status_code} - {response.text}")

    except requests.exceptions.HTTPError as http_err:
        LOGGER.error(f"HTTP error occurred for Job #{job_id}: {http_err}")  # HTTP error
    except requests.exceptions.RequestException as req_err:
        LOGGER.error(f"Request error occurred for Job #{job_id}: {req_err}")  # Other requests-related error
    except Exception as e:
        LOGGER.error(f"An unexpected error occurred for Job #{job_id}: {e}")


def delete_job_artifacts_from_json(jobs_json_file_path, project_id, threshold_before_date):
    """
    Delete job artifacts based on job data from a provided JSON file, filtering by date.

    Parameters:
        jobs_json_file_path (str): Path to the JSON file containing job data.
        threshold_before_date (datetime): Date threshold to delete job artifacts before.
    """
    try:
        # Load job data from JSON file
        with open(jobs_json_file_path, 'r') as file:
            job_data = json.load(file)

    except FileNotFoundError:
        LOGGER.error(f"JSON file not found: {jobs_json_file_path}")
        return
    except json.JSONDecodeError:
        LOGGER.error(f"Error decoding JSON from file: {jobs_json_file_path}")
        return

    # List to store job IDs marked for deletion
    job_ids_marked_delete_artifacts = []

    for job_info in job_data:
        job_id = job_info.get('id')
        
        # Check if job_id is valid
        if not job_id:
            LOGGER.warning(f"Job data does not contain a valid ID: {job_info}")
            continue
        
        # Check if artifacts exist
        artifacts = job_info.get('artifacts', [])
        if not artifacts:
            continue

        # Skip jobs with only job.log artifact
        if len(artifacts) == 1 and artifacts[0]['file_type'] == 'trace' and artifacts[0]['filename'] == 'job.log':
            continue

        created_at = datetime.fromisoformat(job_info['created_at']).replace(tzinfo=None)

        if created_at < threshold_before_date:
            job_ids_marked_delete_artifacts.append(job_id)
            LOGGER.info(f"Job #{job_id} created on {created_at.strftime('%Y-%m-%d')} is older than {threshold_before_date.strftime('%Y-%m-%d')} and its artifacts are marked for deletion.")

    if not job_ids_marked_delete_artifacts:
        LOGGER.info(f"No jobs with artifacts older than {threshold_before_date.strftime('%Y-%m-%d')} found in the JSON file.")
        return

    LOGGER.info(f"Deleting artifacts for {len(job_ids_marked_delete_artifacts)} jobs...")
    for job_id in job_ids_marked_delete_artifacts:
        delete_job_artifacts_by_id(job_id, project_id)

        # Adding a sleep to avoid hitting rate limits
        time.sleep(0.1)


def delete_pipelines(project, threshold_before_date, branches=None, exclude_branches=None):
    """
    Delete pipelines for a given project that are older than the provided date threshold.

    Parameters:
        project (gitlab.v4.objects.Project): The GitLab project object.
        threshold_before_date (datetime): Date threshold to delete pipelines before.
        branches (list): List of branch names to filter pipelines. If None, all branches are included.
        exclude_branches (list): List of branch names to exclude from deletion.
    """
    # List to store pipelines marked for deletion
    pipelines_marked_for_deletion = []
    counter_pipeline = 0

    try:
        # Fetch pipelines with order_by id and sort asc (oldest first)
        # Add ref parameter if branches are specified
        pipeline_params = {
            "pagination": "keyset",
            "order_by": "id", 
            "sort": "asc",
            "per_page": 100,
            "iterator": True
        }

        if not branches and not exclude_branches:
            pipelines = project.pipelines.list(**pipeline_params)
            counter_pipeline = mark_pipelines_for_deletion(pipelines, threshold_before_date, pipelines_marked_for_deletion, counter_pipeline)
        
        if branches:
            for branch in branches:
                pipeline_params["ref"] = branch
                pipelines = project.pipelines.list(**pipeline_params)
                counter_pipeline = mark_pipelines_for_deletion(pipelines, threshold_before_date, pipelines_marked_for_deletion, counter_pipeline, branch)
        
        if exclude_branches:
            pipelines = project.pipelines.list(**pipeline_params)
            branches_to_delete = set()

            # Collect all branches to delete after exclusion
            for pipeline in pipelines:
                if pipeline.ref in exclude_branches:
                    continue
                
                branches_to_delete.add(pipeline.ref)

            for branch in branches_to_delete:
                pipeline_params["ref"] = branch
                pipelines = project.pipelines.list(**pipeline_params)
                counter_pipeline = mark_pipelines_for_deletion(pipelines, threshold_before_date, pipelines_marked_for_deletion, counter_pipeline, branch)

    except gitlab.exceptions.GitlabListError as e:        
        # Skip project if permissions are insufficient
        if e.response_code == 403:
            LOGGER.error(f"Insufficient permissions to fetch pipelines for project {project.name} (ID: {project.id}). Skipping project...")
            return
        
        LOGGER.error(f"An error occurred while fetching pipelines for project {project.name} (ID: {project.id}): {e}")
        return

    if not pipelines_marked_for_deletion:
        LOGGER.info(f"No pipelines older than {threshold_before_date.strftime('%Y-%m-%d')} found in project {project.path_with_namespace}")
        return

    LOGGER.info(f"Deleting {len(pipelines_marked_for_deletion)} pipelines in project {project.path_with_namespace}...")
    for pipeline in pipelines_marked_for_deletion:
        if DRY_RUN:
            LOGGER.info(f"DRY RUN: Would delete Pipeline #{pipeline.id}")
            continue
        
        pipeline.delete()
        LOGGER.info(f"Deleted Pipeline #{pipeline.id}")

        # Adding a sleep to avoid hitting rate limits
        time.sleep(0.1)


def mark_pipelines_for_deletion(pipelines, threshold_before_date, pipelines_marked_for_deletion, counter_pipeline, branch=None):
    """Helper function to mark pipelines for deletion"""
    branch_info = f" for branch '{branch}'" if branch else ""
    
    for pipeline in pipelines:
        counter_pipeline += 1

        created_at = datetime.fromisoformat(pipeline.created_at).replace(tzinfo=None)

        LOGGER.info(f"Checking pipeline #{pipeline.id}{branch_info} created on {created_at.strftime('%Y-%m-%d')} | Total no. of pipelines scanned: {counter_pipeline}")
        
        if created_at < threshold_before_date:
            pipelines_marked_for_deletion.append(pipeline)
            LOGGER.info(f"Pipeline #{pipeline.id}{branch_info} created on {created_at.strftime('%Y-%m-%d')} is older than {threshold_before_date.strftime('%Y-%m-%d')} and is marked for deletion. Pipeline URL: {pipeline.web_url}")
    
    return counter_pipeline


def delete_pipeline_by_id(pipeline_id, project_id, access_token=GITLAB_ACCESS_TOKEN):
    """
    Delete a pipeline using the pipeline ID.

    Parameters:
        pipeline_id (int): The ID of the pipeline to be deleted.
        access_token (str): GitLab access token for authentication.
    """
    url = f"{GITLAB_API_URL}/api/v4/projects/{project_id}/pipelines/{pipeline_id}"
    
    headers = {
        'PRIVATE-TOKEN': access_token
    }

    if DRY_RUN:
        LOGGER.info(f"DRY RUN: Would delete Pipeline #{pipeline_id}")
        return

    try:
        response = requests.delete(url, headers=headers)
        response.raise_for_status()  # Raise an exception for HTTP errors (4xx and 5xx)
        if response.status_code == 204:
            LOGGER.info(f"Deleted Pipeline #{pipeline_id}")
        else:
            LOGGER.error(f"Unexpected response for Pipeline #{pipeline_id}: {response.status_code} - {response.text}")

    except requests.exceptions.HTTPError as http_err:
        LOGGER.error(f"HTTP error occurred for Pipeline #{pipeline_id}: {http_err}")  # HTTP error
    except requests.exceptions.RequestException as req_err:
        LOGGER.error(f"Request error occurred for Pipeline #{pipeline_id}: {req_err}")  # Other requests-related error
    except Exception as e:
        LOGGER.error(f"An unexpected error occurred for Pipeline #{pipeline_id}: {e}")


def delete_pipelines_from_json(pipelines_json_file_path, project_id, threshold_before_date):
    """
    Delete pipelines based on pipeline data from a provided JSON file, filtering by date.

    Parameters:
        pipelines_json_file_path (str): Path to the JSON file containing pipeline data.
        threshold_before_date (datetime): Date threshold to delete pipelines before.
    """
    try:
        # Load pipeline data from JSON file
        with open(pipelines_json_file_path, 'r') as file:
            pipeline_data = json.load(file)

    except FileNotFoundError:
        LOGGER.error(f"JSON file not found: {pipelines_json_file_path}")
        return
    except json.JSONDecodeError:
        LOGGER.error(f"Error decoding JSON from file: {pipelines_json_file_path}")
        return

    # List to store pipeline IDs marked for deletion
    pipeline_ids_marked_for_deletion = []

    for pipeline_info in pipeline_data:
        pipeline_id = pipeline_info.get('id')
        
        # Check if pipeline_id is valid
        if not pipeline_id:
            LOGGER.warning(f"Pipeline data does not contain a valid ID: {pipeline_info}")
            continue

        created_at = datetime.fromisoformat(pipeline_info['created_at']).replace(tzinfo=None)

        if created_at < threshold_before_date:
            pipeline_ids_marked_for_deletion.append(pipeline_id)
            LOGGER.info(f"Pipeline #{pipeline_id} created on {created_at.strftime('%Y-%m-%d')} is older than {threshold_before_date.strftime('%Y-%m-%d')} and is marked for deletion.")

    if not pipeline_ids_marked_for_deletion:
        LOGGER.info(f"No pipelines older than {threshold_before_date.strftime('%Y-%m-%d')} found in the JSON file.")
        return

    LOGGER.info(f"Deleting {len(pipeline_ids_marked_for_deletion)} pipelines...")
    for pipeline_id in pipeline_ids_marked_for_deletion:
        delete_pipeline_by_id(pipeline_id, project_id)

        # Adding a sleep to avoid hitting rate limits
        time.sleep(0.1)


def delete_container_images(project, threshold_before_date):
    """
    Delete container images for a given project based on the provided date threshold,
    and calculate the total size of the tags deleted.

    Parameters:
        project (gitlab.v4.objects.Project): The GitLab project object.
        threshold_before_date (datetime): Date threshold to delete images before.
    """
    try:
        # Fetch the list of image repositories for the project
        image_repositories = project.repositories.list(pagination="keyset", per_page=100, iterator=True)
    except gitlab.exceptions.GitlabListError as e:
        # Handle insufficient permissions or other errors
        if e.response_code == 403:
            LOGGER.error(f"Insufficient permissions to fetch image repositories for project {project.name} (ID: {project.id}). Skipping project...")
            return
        
        LOGGER.error(f"An error occurred while fetching image repositories for project {project.name} (ID: {project.id}): {e}")
        return
    
    for image_repository in image_repositories:
        try:
            # Fetch all tags in the repository
            all_tags = image_repository.tags.list(pagination="keyset", per_page=100, iterator=True)

        except gitlab.exceptions.GitlabListError as e:
            LOGGER.error(f"Error fetching tags for repository {image_repository.path} in project {project.name}: {e}")
            continue

        # List to store tags marked for deletion
        tags_marked_for_deletion = []
        # Total size of tags deleted
        total_deleted_size = 0

        for tag in all_tags:
            try:
                # Get the tag details to access its created_at and total_size attributes
                tag_object = image_repository.tags.get(id=tag.name)
                created_at_str = tag_object.created_at
                tag_size = tag_object.total_size or 0  # Default to 0 if size is not available
                
                # Parse the created_at string into a datetime object and remove timezone info
                created_at = datetime.strptime(created_at_str, '%Y-%m-%dT%H:%M:%S.%f%z').replace(tzinfo=None)

                # Check if the tag was created before the threshold date
                if created_at < threshold_before_date:
                    tags_marked_for_deletion.append(tag)
                    total_deleted_size += tag_size
                    LOGGER.info(f"Tag {tag.name} created on {created_at.strftime('%Y-%m-%d')} is older than {threshold_before_date.strftime('%Y-%m-%d')} and is marked for deletion. Image repository: {image_repository.path}")
            except gitlab.exceptions.GitlabGetError as e:
                LOGGER.error(f"Error fetching details for tag {tag.name} in repository {image_repository.path}: {e}")
                continue
        
        if not tags_marked_for_deletion:
            LOGGER.info(f"No tags older than {threshold_before_date.strftime('%Y-%m-%d')} found in image repository {image_repository.path} of project {project.name}")
            continue
        
        if tags_marked_for_deletion:
            LOGGER.info(f"Deleting {len(tags_marked_for_deletion)} tags in image repository {image_repository.path} of project {project.name}...")
            for tag in tags_marked_for_deletion:
                if DRY_RUN:
                    LOGGER.info(f"DRY RUN: Would delete tag {tag.name}")
                    continue
                
                tag.delete()
                LOGGER.info(f"Deleted tag {tag.name}")

        readable_size = convert_size_to_human_readable(total_deleted_size)
        LOGGER.info(f"Total size of deleted tags: {readable_size}")


def get_all_projects_with_min_access_level(gitlab_client, min_access_level=MIN_ACCESS_LEVEL):
    """
    Retrieve all projects from the GitLab instance with a minimum access level.

    Parameters:
        gitlab_client (gitlab.Gitlab): Authenticated GitLab client.
        min_access_level (int): Minimum access level required to access the project. 10 = Guest, 20 = Reporter, 30 = Developer, 40 = Maintainer, 50 = Owner.

    Returns:
        list: A list of GitLab project objects.
    """
    try:
        projects = gitlab_client.projects.list(pagination="keyset", per_page=100, iterator=True, min_access_level=min_access_level)
        return projects
    except gitlab.exceptions.GitlabListError as e:
        LOGGER.error(f"Failed to retrieve project(s): {e}")
        return []


def get_project_by_id(gl, project_id):
    """
    Retrieve a project based on ID from the GitLab instance.

    Parameters:
        gitlab_client (gitlab.Gitlab): Authenticated GitLab client.
        project_id (int): The project ID.

    Returns:
        gitlab.v4.objects.Project: The GitLab project object.
    """
    try:
        project = gl.projects.get(project_id)
        return project
    except gitlab.exceptions.GitlabGetError as e:
        LOGGER.error(f"Failed to retrieve project {project_id}: {e}")
        return None


def main(args):
    """
    Main function to perform the cleanup operations based on the provided arguments.
    """
    gitlab_client = initialize_script_parameters(args)
    threshold_before_date = parse_date(args.date)

    if args.branches and args.exclude_branches:
        LOGGER.warning("Please use either --branches or --exclude-branches and try again.")

    # Cleanup based on project ID or JSON file
    if args.json:
        cleanup_project_from_json(args.json, args.project, args.cleanup, threshold_before_date)
    else:
        retrieve_and_cleanup_projects(gitlab_client, args.cleanup, threshold_before_date, args.branches, args.exclude_branches)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cleanup GitLab artifacts and registry images based on the provided date threshold. Contact: https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service")
    # Uncomment later to provide the option to cleanup all projects
    # parser.add_argument('--project', type=str, required=True, help="Project ID or 'all' to cleanup all projects")
    parser.add_argument('--project', help='Project ID to cleanup')
    parser.add_argument('--json', type=str, help='Path to the JSON file containing data to delete job artifacts or pipelines')
    parser.add_argument('--cleanup', choices=['artifacts', 'images', 'pipelines', 'all'], required=True, 
                        help='Specify which resources to delete: artifacts, images, pipelines or all.')
    parser.add_argument('--date', required=True, help='The date threshold to delete resources before (format: YYYY-MM-DD).')
    parser.add_argument('--token', help='GitLab access token for authentication. Can be set as GITLAB_ACCESS_TOKEN environment variable. Token scope required: api, read_api.')
    parser.add_argument('--dry-run', action='store_true', help='Perform a dry run without deleting anything.')
    parser.add_argument('--branches', nargs='+', help='Specific branches to delete pipelines from. Default is all branches. (format: main dev stg)')
    parser.add_argument('--exclude-branches', nargs='+', help='Branches to exclude from pipeline deletion. (format: main dev stg)')

    args = parser.parse_args()
    main(args)
